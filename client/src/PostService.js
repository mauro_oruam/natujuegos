import axios from 'axios';

// Es importante la '/' final como estandar con las url en la api
const url ='api/posts/';

class PostService {
  // Get Posts
  // el static permite que no haga falta instanciar un objeto de la clase, en
  // este caso PostService, para llamar a la función, ej: PostService.getPosts()
  static getPosts() {
    return new Promise(async (resolve, reject) => {
      try {
        // Intentamos pedir los posts
        const res = await axios.get(url);
        // nos quedamos con la data de la response
        const data = res.data;
        // finalmente usamos resolve para usar la data
        resolve(
          // map iterará un array y creará uno nuevo con los cambios elegidos
          data.map(post => ({
            // usamos un spread operator
            ...post,
            // recuperamos la fecha de creación de cada post
            createdAt: new Date(post.createdAt)
          }))
        );
        // Si hay un error la Promesa se rechaza con reject en el bloque catch
      } catch(err) {
        reject(err);
      }
    });
  }

  // Create Post
  static insertPost(text) {
    return axios.post(url, {
      // "text: text" puede ser resumido en ES6, poniendo solo un "text"
      text
    });
  }
  // Delete Post
  static deletePost(id) {
    return axios.delete(`${url}${id}`);

  }



}
export default PostService;
