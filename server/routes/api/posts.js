const express = require('express');

const router = express.Router();

// Get posts (la '/ no refiere al local-host/home sino a lo que se haya derivado')
// Por ejemplo en este caso deriva a './routes/api/posts'
router.get('/', async (req, res) => {
  const posts = await loadPostCollection();
  // entre las llaves se pueden hacer querys
  res.send(await posts.find({}).toArray());
});

// Add posts
router.post('/', async (req, res) => {
  const posts = await loadPostCollection();
  await posts.insertOne({
    text: req.body.text,
    createdAt: new Date()
  });
res.status(201).send();
});


// Delet Post
router.delete('/:id', async (req, res) => {
  const posts = await loadPostCollection();
  await posts.deleteOne({ _id: new mongodb.ObjectID(req.params.id) }, function (err, results) {
  });
  res.status(200).send();
});


async function loadPostCollection() {
  const client = await mongodb.MongoClient.connect
  ('mongodb+srv://mauro:merliniox22@cluster0-64td1.mongodb.net/natujuegos', {
    useNewUrlParser: true
  });
  return client.db('natujuegos').collection('players')
}


module.exports = router;
