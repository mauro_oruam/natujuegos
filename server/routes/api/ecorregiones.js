const express = require('express');
const _ = require('lodash');

const router = express.Router();
const Ecorregion = require('../../../models/ecorregion')

// Get ECORREGION
router.get('/:regions/:rarity', async (req, res) => {
  try {
    let selected = req.params.regions.split(',')
    let rarity = req.params.rarity

    //const birds_pack = await Ecorregion.find({$or:[{regions:selected}, {regions:'todo_el_pais'}], rarity:5 })
    const birds_pack_crudo = await Ecorregion.find({
      regions: {
        $in: selected
      },
      rarity: {
        $gte: rarity
      }
    })



    function filtrarPorPhoto(obj) {
      if (obj.photo != null) {
        return obj;
      }
    }

    const birds_pack_con_fotos = birds_pack_crudo.filter(filtrarPorPhoto);

    //console.log(birds_pack.length)
    let question_pack = createQuestionPack(birds_pack_con_fotos, rarity)
    res.json(question_pack)
  } catch (err) {
    res.status(500).json({
      messaje: err.message
    })
  }
})


// función que arma los paquetes de n aves para preguntas y devuelve n
// preguntas con sus respuestas en un json
function createQuestionPack(input_pack, rar) {
  let animalSelection = []
  let input = input_pack
  let rarity = rar
  // Creamos template de paquete de preguntas
  let question_pack_template = {
    "response_code": 0,
    "results": []
  };



  // Elegimos las respuestas correctas
  if (input.length >= 5) {
    animalSelection = _.sampleSize(input, [n = 5])
  } else {
      animalSelection = _.sampleSize(input, [n = 5])
      for (let i = animalSelection.length; i < 5; i++) {
        animalSelection.push(animalSelection[i - 2])
        //console.log(animalSelection.length)
    }
  }
  //
  for (let i = 0; i < 5; i++) {
    // Creamos template de pregunta
    let question_template = {
      "category": "",
      "type": "",
      "rarity": "",
      "photos": [],
      "correct_answer": "",
      "incorrect_answers": "",
      "author": ""
    }

    let answer = animalSelection[i]


    question_template["categoty"] = "animals";
    question_template["correct_answer"] = answer.name

    // respuestas incorrectas
    question_template["incorrect_answers"] = _.sampleSize(input.map(a => a.name)
      .filter(input =>
        input !== question_template["correct_answer"]), [n = 4])
    question_template["incorrect_answers"] = [...new Set(question_template["incorrect_answers"])]
    question_template["rarity"] = rarity
    // agregar en el model la key urlOriginal para que pueda enviarse
    question_template["author"] = answer.urlOriginal



    //respuestas incorrectas que se repiten (más facil)
    //question_template["incorrect_answers"] = animalSelection.map(a => a.name)
    //  .filter(animalSelection =>
    //    animalSelection !== question_template["correct_answer"])
    //question_template["incorrect_answers"] = [...new Set(question_template["incorrect_answers"])]
    //question_template["rarity"] = rarity

    // photos
    if (process.env.NODE_ENV === 'production') {
      if (answer.photo == null) {
        question_template["photos"] = '/images/birds/404.jpg';
      } else if (answer.photo !== '') {
        question_template["photos"] = answer.photo.replace('/img/', '/images/');
      }
    } else {
      if (answer.photo != null) {
        question_template["photos"] = answer.photo.replace('/img/', 'http://localhost:5000/images/');
      } else if (answer.photo == null) {
        question_template["photos"] = 'http://localhost:5000/img/404.jpg';
      }
    }

    question_pack_template.results.push(question_template)
  }

  return question_pack_template

}






module.exports = router
