const mongoose = require('mongoose')

const ecorregionSchema = new mongoose.Schema({
  name: {
    type: String
  },
  author: {
    type: String
  },
  rarity: {
    type: Number
  },
  regions: {
    type: Array
  },
  photo: {
    type: String
  },
  urlOriginal: {
    type: String
  }
}, {
  collection: 'aves'
})

module.exports = mongoose.model('ecorregion', ecorregionSchema)
